#include <stdio.h>
#include <math.h>
#include <pthread.h> 
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
void *mult(void* arg) // Esta función correspode al proceso que debe realizar cada thread para calcular la entrada de la matriz
{ 
    int *data = (int *)arg; 
    int k = 0, i = 0; 
      
    int x = data[0]; 
    for (i = 1; i <= x; i++) 
           k += data[i]*data[i+x]; 
      
    int *p = (int*)malloc(sizeof(int)); 
         *p = k; 
      
//Se termina el thread y el resultado se pasa como puntero 
    pthread_exit(p); 
} 

void printV(int* matriz, int n){ // Función para imprimir vectores
    for(int i = 0; i < n; i++){
        printf("%d ", matriz[i]);
    }
    printf("\n");
}

void printMat(int** matrix, int n){ // Función para imprimir la matriz nxn
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            printf("%d ", matrix[i][j]);
            if (j == n-1){
                printf("\n");
            }
        }
    }
}

int* matrizSerie(int* vector, int n , int** matrix){ // Calculo de la matriz en serie

   
    int* result = calloc(n, sizeof(int));
    for(int a = 0; a < n; a++){ //Un doble for para recorrer filas y columnas y realizar la multiplicación
        for (int b = 0; b < n; b++)
        {
           result[a] += vector[b] * matrix[b][a];
        }
        
    }
    // Imprimir
    printf("El resultado de la matriz serial es: ");
    printV(result, n);
  return result;
}

int *matrizP(int* vector, int n , int** matrix){ // Calculo de la matriz en paralelo
    pthread_t *threads; 
    threads = (pthread_t*)malloc(n*sizeof(pthread_t)); //La cantidad de threads va a depender del tamaño del vector resultante donde n es la cantidad de columnas

    int count = 0;
    int k; 
    int i,j;
    int *p;
    int *c;
    c = malloc(n*sizeof(int));
    int* data = NULL;  
    for (j = 0; j < n; j++){ 
                
        //Se guardan los valores de fila y columna en data 
        data = (int *)malloc((20)*sizeof(int)); 
        data[0] = n; 
        
        for (k = 0; k < n; k++) 
            data[k+1] = vector[k]; 
    
        for (k = 0; k < n; k++) 
            data[k+n+1] = matrix[k][j]; 
            
            //Se crean los threads
            pthread_create(&threads[count++], NULL,  
                            mult, (void*)(data)); 
                
    }

    printf("El resultado de la matriz con Paralelismo es: "); 
    for (i = 0; i < n; i++){ 
    void *k; 
    
    //Se juntan todos los threads y se recoge el valor de retorno  
    pthread_join(threads[i], &k); 
             
            
    int *p = (int *)k;

    c[i] = *p;
     
    //printf("%d ",*p);
    }  
    //printf("\n");
    
    return c;
  
  
}


int comparematrix(){ // Funcion para comparar los resultados en paralelo vs serie
    printf("-------------------------------------- \n");
    printf("Ejercicio Matrices: \n");
    printf("-------------------------------------- \n");
    int n = rand()%6+3; // n es un valor entre 3 y 8
    int vector[n];
    srand(time(NULL));
    int** matrix;
    matrix = malloc(n*sizeof(int*));
    // Se rellenan tanto el vector como la matriz con valores aleatorios
    for (int i = 0; i < n; i++){
        matrix[i] = malloc(n*sizeof(int));
    }
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            matrix[i][j] = rand()%100;
        }
    }
    int i, j;
    for (i = 0; i < n; i++)
    {
        vector[i] = rand()%100;
    }
    printf("El vector a multiplicar es: \n");
    printV(vector, n);
    printf("La matriz a multiplicar es: \n");
    printMat(matrix, n);
    int*result=matrizSerie(vector, n, matrix);
    int*result2=matrizP(vector, n, matrix);
    printV(result2,n);


    for(int i = 1; i <= n; i++) { // Se comparan los resultados entrada por entrada de ambos vectores que retornan el resultado en serie y paralelo.
      if (result[i] != result2[i]) {
 
      return -1;
    }
    else {
    return 0;
    }
} 
}