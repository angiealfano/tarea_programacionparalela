

#include <stdio.h>
#include <math.h>
#include <pthread.h> 
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

int main(){
    
    int resultpi = comparepi();
    int resultmatrix = comparematrix ();
    if (resultpi == 0 && resultmatrix == 0){
        printf("--------------------------------\n");
        printf("Los resultados de ambos ejercicios coinciden en la forma serial y paralela \n");
        return 0;
    }
    else{
        printf("--------------------------------\n");
        printf("Los resultados de ambos ejercicios NO coinciden en la forma serial ni paralela\n ");
        return -1;
    }
}