
#include <stdio.h>
#include <math.h>
#include <pthread.h> 
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#define NUM_THREADS 5
#define ITERACIONES 100000
/*Usando Formula Leibniz*/
float valorpi;
float calcPiSerial (int iteraciones){
    int i;
    valorpi = 0;

    for (i = 0; i < iteraciones; i++){
        valorpi +=   4*  ((pow(-1, i))/(2*i +1));   
    }
    /*Este valor se trunco, ya que el ultimo digito de precision no coincidia por asuntos de la formula utilizada
    si se usa un valor de iteraciones menor a mil no es necesario truncar, el resultado si da igual, si se comenta se podra ver 
    que es el ultimo digito el que difiere*/
    valorpi = floor(valorpi * 100000) / 100000; 

    printf("El valor de pi serial utilizando %i iteraciones es: %f \n",iteraciones, valorpi);
    return valorpi;
}
 
 void *calcPiParalelo (void *ID){
    int actualthread =  (int)ID;
    int iter_thread = ITERACIONES / NUM_THREADS;  //Valores necesarios para poder hacer iteracion sobre threads
    int primera_iter = actualthread * iter_thread;
    int ultima_iter = primera_iter + iter_thread;
    double resultadothread;
    pthread_mutex_t mutex;

    for (int i = primera_iter; i < ultima_iter; i++){
        resultadothread += 4* ((pow(-1, i))/(2*i +1));      
    }
   
    pthread_mutex_lock(&mutex);
      valorpi += resultadothread;
    pthread_mutex_unlock(&mutex);
} 


int comparepi(){
    printf("-------------------------------------- \n");
    printf ("Ejercicio Pi: \n");
    printf("-------------------------------------- \n");
    float piserial;
    piserial = calcPiSerial(ITERACIONES);

    /*Inicializamos los threads que queremos*/
     valorpi = 0;
     pthread_t threads [NUM_THREADS];

     for (int i = 0; i < NUM_THREADS; i++) {
         pthread_create(&threads[i], NULL, calcPiParalelo, (void *)i); 
         
     }
    /*Se realiza el join para obtener el valor de retorno*/
     for (int i = 0; i < NUM_THREADS; i++) {
             pthread_join(threads[i], NULL);   
     }

    valorpi =floor(valorpi * 100000) / 100000;
     printf("El valor de pi paralelo utilizando %i iteraciones es: %f \n",ITERACIONES, valorpi);      
    if (piserial == valorpi){
        return 0;
    }
    else {
        return -1; //debe ser negativo para que el pipeline de git lo entienda
    }

}



